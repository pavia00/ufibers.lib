unit UFibers;

interface

Uses Windows;

type
 TFiberCPUState= record
   _EBX:Pointer;
   _EIP:Pointer;
   _ESP:Pointer;
   _EBP:Pointer;
   end;
 TFibersDesc=record
   ID:Integer;
   StackBase:Pointer;
   StackSize:DWord;
   State:TFiberCPUState;
   IsFromThread:Boolean;
   end;
 TFibers = class
 private
    Owner:TObject;
    FibersList:array of TFibersDesc;
    FCount: Integer;
    procedure SetCount(const Value: Integer);
 protected
    function  InitStack(_ESP:Pointer; ThreadState:TFiberCPUState):Integer; stdcall;
    function  CreatFiber(aId:integer; ThreadState:TFiberCPUState):TFibersDesc;
    function  CreatFiberFromThread(aId:integer; ThreadState:TFiberCPUState):TFibersDesc;
    procedure DestroyFiber(Fiber:TFibersDesc);

 public
   ThreadState:TFiberCPUState;
   CurrentFiberID:Integer;
   constructor Create(aOwner:TObject);
   destructor Destroy;
   property Count:Integer read FCount write SetCount;
   procedure Yield();
 published

 end;

var
  MainState:TFiberCPUState;

function GetState(self:Pointer):TFiberCPUState; register;
//procedure Yield();
//procedure Fibers(count:Integer);

implementation


const defultStakSize=64*1024;


var
  cPageSize:DWord;

function GetPageSize:DWord;
var sysInfo:TSystemInfo;
begin
if cPageSize=0 then
  begin
  GetSystemInfo(sysInfo);
  cPageSize:=sysInfo.dwAllocationGranularity;
  end;
  result:=cPageSize;
end;

function GetStack(aStakSize:DWord):Pointer;
var
  PageSize:DWord;
  GP:Pointer;
begin
  PageSize:= GetPageSize;   
  {$IF DEBUG}  assert(aStakSize mod PageSize=0,'������ ����� ������ ���� ������ ��������'); {$IFEND}
  GetMem(result,aStakSize);
  {
  GP:=VirtualAlloc(Nil, 2*PageSize+aStakSize,MEM_RESERVE + MEM_TOP_DOWN,PAGE_READWRITE);
  GP:=VirtualAlloc(GP, PageSize,MEM_COMMIT,PAGE_READONLY);
  INC(PByte(GP),PageSize);
  GP:=VirtualAlloc(GP, aStakSize,MEM_COMMIT,PAGE_READWRITE);
  Result:=GP;
  INC(PByte(GP),aStakSize);
  GP:=VirtualAlloc(GP, PageSize,MEM_COMMIT,PAGE_READONLY);}
end;

procedure FreeStack(Stack:Pointer; aStakSize:DWord);
var
  PageSize:DWord;
  GP:Pointer;
  ck:Boolean;
begin
  FreeMem(Stack,aStakSize);
{  PageSize:= GetPageSize;
  GP:=Stack;
  Dec(PByte(GP),PageSize);
  ck:=VirtualFree(GP,0, MEM_RELEASE	);{}
end;

// ������ ������������� ���������
function TFibers.InitStack(_ESP:Pointer; ThreadState:TFiberCPUState):Integer;
var State:TFiberCPUState;
begin
State:=GetState(nil);
if CurrentFiberID=0 then
   begin
   Dec(PByte(_ESP),SizeOf(Pointer));
   Result:=SizeOf(Pointer);
//   Log(':ThreadState._EIP=%p;',[ThreadState._EIP]);

   PPointer(_ESP)^:=ThreadState._EIP; {}
{   Result:=Integer(ThreadState._ESP)-Integer(State._ESP);
   Dec(PByte(State._ESP^),SizeOf(State));
   Dec(PByte(_ESP),Result+SizeOf(State));
   Move(State._ESP^ ,_ESP^, Result+SizeOf(State)); {}
{   Result:=Integer(ThreadState._ESP)-Integer(State._ESP);
   Dec(PByte(State._ESP^),SizeOf(State));
   Dec(PByte(_ESP),Result);
   Move(State._ESP^ ,_ESP^, Result); {}
   end else
     result:=0;
end;

function GetState(self:Pointer):TFiberCPUState;
assembler
asm
  mov ecx, EAX
  mov [result._ebx],ECX
  mov ecx, EBP
  mov [result._ebp],ECX
  mov ecx, ESP
  mov [result._esp],ECX
  mov ecx, [ESP]
  mov [result._eip],ECX
end;

procedure SetState(State:TFiberCPUState; var OutState:TFiberCPUState);
assembler
asm
  mov ecx, EBX
  mov [OutState._ebx],ECX
  mov ecx, EBP
  mov [OutState._ebp],ECX
  mov ecx, ESP
  mov [OutState._esp],ECX
  mov ecx, [ESP]
  mov [OutState._eip],ECX

  mov ebx, [State._EBX]
  mov ebp, [State._EBP]
  mov ecx, [State._ESP]
  mov eax, [ecx]
  mov ESP,ECX
  mov eax, ebx
end;

{ TFibers }

function TFibers.CreatFiber(aId:integer; ThreadState:TFiberCPUState):TFibersDesc;
var Size:Integer;
begin
  result.StackBase:=GetStack(defultStakSize);
  result.State:=ThreadState;
  result.State._ESP:=result.StackBase;
  inc(PByte(result.State._ESP),defultStakSize);
  result.ID:=aID;
  result.StackSize:=defultStakSize;
  Size:=InitStack(result.State._ESP, ThreadState);
  dec(PByte(result.State._ESP),Size);
//  result.State._EIP:=PPointer(result.State._ESP)^;
  result.IsFromThread:=False;
end;

function TFibers.CreatFiberFromThread(aId:integer; ThreadState:TFiberCPUState):TFibersDesc;
var
 State:TFiberCPUState;
begin
  State:=GetState(Owner);
  result.StackBase:=ThreadState._ESP;
  result.State:=ThreadState;
  result.State._ESP:=State._ESP;
  result.ID:=aID;
  result.StackSize:=0;
  result.State._EIP:=PPointer(result.State._ESP)^;
  result.IsFromThread:=True;
end;


procedure TFibers.DestroyFiber(Fiber:TFibersDesc);
begin
  if not Fiber.IsFromThread then
    FreeStack(Fiber.StackBase, Fiber.StackSize);
end;

constructor TFibers.Create(aOwner:TObject);
begin
  Owner:=aOwner;
  ThreadState:=GetState(Owner);
end;

destructor TFibers.Destroy;
begin
  Count:=0;
  inherited;
end;

procedure TFibers.SetCount(const Value: Integer);
var LastCount,i:Integer;
begin
  FCount := Value;
  LastCount:=Length(FibersList);
  for i:=FCount to LastCount-1 do
      DestroyFiber(FibersList[i]);
  SetLength(FibersList,Count);
  for i:=LastCount to FCount-1 do
      if CurrentFiberID=0 then
         begin
         if i=0 then
              FibersList[i]:=CreatFiberFromThread(i, ThreadState)
            else
              FibersList[i]:=CreatFiber(i, ThreadState);
         end;
  if (CurrentFiberID<0) then CurrentFiberID:=0;
end;

// �������� ��������� ��������� ��������.
procedure TFibers.Yield;
var OldID:Integer;
begin
  OldID:=CurrentFiberID;
  CurrentFiberID:=(CurrentFiberID+1) mod Length(FibersList);
  SetState(FibersList[CurrentFiberID].State,FibersList[OldID].State);
end;

begin

end.

