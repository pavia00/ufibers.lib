program Test;

{$APPTYPE CONSOLE}

uses
  SysUtils, Classes,
  UFibers in '..\src\UFibers.pas';

type

  TThreadWoker = class(TThread)
   private
    Fibers:TFibers;
   protected

   public
    FibersCount:Integer;
    StringList:TStringList;
    constructor Create;
    destructor Destroy;
    procedure Execute;  override;
   published

   end;

var TestResult:record Total,Good,bed:Integer; end;

{ TThreadWoker }

constructor TThreadWoker.Create;
begin
 StringList:=TStringList.Create;
  FibersCount:=1;
 inherited Create(True);
 Fibers:=TFibers.Create(self);
end;

destructor TThreadWoker.Destroy;
begin
  Fibers.Destroy;
  StringList.Destroy;
  inherited Destroy;
end;

procedure TThreadWoker.Execute;
begin
  // Init Fibers
  Fibers.ThreadState:=GetState(self);
  Fibers.Count:=FibersCount;
  // Execute
  StringList.Add('Fiber ID'+IntToStr(Fibers.CurrentFiberID));
  Fibers.Yield;
  StringList.Add('Fiber ID'+IntToStr(Fibers.CurrentFiberID));
  if Fibers.CurrentFiberID<>0 then
     Fibers.Yield;
  StringList.Add('Fiber ID'+IntToStr(Fibers.CurrentFiberID));
  if Fibers.CurrentFiberID<>2 then
     Fibers.Yield;
  StringList.Add('Fiber ID'+IntToStr(Fibers.CurrentFiberID));
  Fibers.Yield;

  // Terminate Fibers
  Fibers.Count:=0;

end;

// Test compilator

function CheckRun:boolean;
var Woker:TThreadWoker;
begin
 Woker:=TThreadWoker.Create;
 Woker.FibersCount:=3;
 Woker.Resume;
 Woker.WaitFor;
 result:=Woker.StringList.Text
    = 'Fiber ID0'#$D#$A
    + 'Fiber ID1'#$D#$A
    + 'Fiber ID2'#$D#$A
    + 'Fiber ID0'#$D#$A
    + 'Fiber ID0'#$D#$A
    + 'Fiber ID1'#$D#$A
    + 'Fiber ID2'#$D#$A
    + 'Fiber ID0'#$D#$A
    + 'Fiber ID1'#$D#$A
    + 'Fiber ID2'#$D#$A
    + 'Fiber ID2'#$D#$A
    ;
 Woker.Destroy;
Inc(TestResult.Total);
if result=true then 
     Inc(TestResult.Good)
  else 
      Inc(TestResult.Bed);
end;

begin
  { TODO -oUser -cConsole Main : Insert code here }
  ExitCode:= ord(CheckRun=True);

  WriteLn('Test coverage');
  WriteLn(Format('Test resul: Total %d good %d bed %d',[TestResult.Total, TestResult.Good, TestResult.Bed]));
end.
 